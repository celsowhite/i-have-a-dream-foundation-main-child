<?php

	/*======================
	Save ACF program data to JSON file
	JSON file is used to create the map of data points.
	======================*/
	
	function program_data_updated($post_id) {

		// Only Run if on the 'Our Network' page

		if($post_id === 230) {

			// Set up base program array

			$programs = array(
				'active_programs' => [],
				'past_programs' => []
			);

			// Loop through each of the current programs to add them to our list

			if(have_rows('ihdf_programs')): while(have_rows('ihdf_programs')): the_row();

				$name = get_sub_field('name');

				$status = get_sub_field('status');

				$description = get_sub_field('description');

				$state = get_sub_field('state_initials');

				$lat = get_sub_field('latitude');

				$long = get_sub_field('longitude');

				$image = get_sub_field('image')['sizes']['thumbnail'];

				$website = get_sub_field('website');

				$facebook = get_sub_field('facebook');

				$twitter = get_sub_field('twitter');

				$linkedin = get_sub_field('linkedin');

				$instagram = get_sub_field('instagram');

				$program_array = array(
					'name'        => $name,
					'status'      => $status,
					'description' => $description,
					'state'       => $state,
					'lat'         => $lat,
					'long'        => $long,
					'img'         => $image,
					'website'     => $website,
					'facebook'    => $facebook,
					'twitter'     => $twitter,
					'linkedin'    => $linkedin,
					'instagram'   => $instagram
				);

				array_push($programs['active_programs'], $program_array);

			endwhile; endif;

			// Loop through each of the states to add them to our list

			if(have_rows('past_program_states')): while(have_rows('past_program_states')): the_row();

				$state = get_sub_field('state_initials');

				array_push($programs['past_programs'], $state);

			endwhile; endif;

			// Add JSON formatted data to our json file.

			file_put_contents(get_stylesheet_directory() . '/js/data/programs.json', json_encode($programs));
		}
	}
	
	add_action('save_post', 'program_data_updated');

?>
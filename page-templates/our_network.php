<?php
/*
Template Name: Our Network
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container">

					<!-- Content -->

					<div class="wysiwyg">
						<?php the_content(); ?>
					</div>

					<!-- Interactive Map -->

					<?php
					/*
					<ul class="map_toggles">
						<li class="current_programs_switch active">Present</li>
						<li class="past_programs_switch">Alumni</li>
					</ul>
					*/
					?>

					<div class="map_description_toggle wysiwyg">
						<div class="current"><?php the_field('current_programs_description'); ?></div>
						<div class="past"><?php the_field('past_programs_description'); ?></div>
					</div>

					<div class="map_container">
						<div id="map"></div>
						<div class="new_zealand"><img src="<?php echo get_stylesheet_directory_uri() . '/img/kartograph/new_zealand.svg'; ?>" /></div>
						<div id="tooltips_list"></div>
					</div>

					<!-- Program List -->

					<ul class="current_program_list">

						<?php

						// Blank array to log city names as we loop through them

						$city_names = [];

						if(have_rows('ihdf_programs')): while(have_rows('ihdf_programs')): the_row();

						$name = get_sub_field('name');

						$status = get_sub_field('status');

						$city = get_sub_field('city');

						$state = get_sub_field('state_initials');

						$cityStateCombo = $city . ' ' . $state;

						// Check if we have already logged this city.
						// If so then mark it as a duplicate city.
						// This is so we don't show the title when there are multiple programs in the same city.

						if(in_array($cityStateCombo, $city_names)) {
							$duplicateCity = true;
						}
						else {
							$duplicateCity = false;
							// Because it wasn't found add it to our city name data
							array_push($city_names, $cityStateCombo);
						}

						$lat = get_sub_field('latitude');

						$long = get_sub_field('longitude');

						$image = get_sub_field('image')['sizes']['thumbnail'];

						$description = get_sub_field('description');

						$website = get_sub_field('website');

						$facebook = get_sub_field('facebook');

						$twitter = get_sub_field('twitter');

						$linkedin = get_sub_field('linkedin');

						$instagram = get_sub_field('instagram');

						?>

							<?php if($status === 'current'): ?>
								<li>
									<?php
									// If city hasn't already been listed then show.
									if(!$duplicateCity): ?>
										<h2><?php 
										if($state) echo $city . ', ' . $state;
										else echo $city;
										?></h2>
									<?php endif; ?>
									<h3 class="teal_text"><?php echo $name; ?></h3>
									<?php if($description): ?>
										<p><?php echo $description; ?></p>
									<?php endif; ?>
									<?php if($website): ?>
										<a class="underlined_link" href="<?php echo $website; ?>">Website</a>
									<?php endif; ?>
								</li>
							<?php endif; ?>

						<?php endwhile; endif; ?>

					</ul>

				</div>

				<!-- Program Staff Testimonials -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>

				<!-- News From Our Network -->

				<div class="container">
					<h2 class="text_center">News from our Network</h2>

					<div class="flexslider ihdf_post_carousel">
						<ul class="slides">
							<?php
							$network_news_loop_args = array (
								'post_type'       => 'post', 
								'category_name'   => 'ihdf-network', 
								'posts_per_page'  => 9,
								'order'           => 'DESC'
							);
							$network_news_loop = new WP_Query($network_news_loop_args);
							if ($network_news_loop -> have_posts()) : while ($network_news_loop -> have_posts()) : $network_news_loop -> the_post();
							?>	

								<li>
									<div class="ihdf_post_carousel_content">
										<?php if(has_post_thumbnail()): ?>
											<a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('large_thumbnail'); ?>" /></a>
										<?php endif; ?>
										<h3><?php the_title(); ?></h3>
										<h4><?php echo category_terms_list($post->ID, 'ihdf_post_affiliate'); ?></h4>
										<p><?php the_excerpt(); ?></p>
										<?php if(has_category('press')): ?>
											<a href="<?php the_field('publication_link')?>" target="_blank" class="underlined_link">Read More</a>
										<?php else: ?>
											<a href="<?php the_permalink(); ?>" class="underlined_link">Read More</a>
										<?php endif; ?>
									</div>
								</li>

							<?php endwhile; wp_reset_postdata(); endif; ?>
						</ul>
					</div>
				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>


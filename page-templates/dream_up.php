<?php
/*
Template Name: Dream Up
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container wysiwyg">
					<?php the_content(); ?>
				</div>

				<!-- Testimonial Slider -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>


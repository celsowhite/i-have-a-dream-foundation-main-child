<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<!-- Featured Posts Slider -->

			<?php get_template_part('template-parts/featured_slider'); ?>

			<div class="page_content">

				<div class="container wysiwyg">
					<?php the_content(); ?>
				</div>

				<!-- Masonry Testimonials -->

				<?php get_template_part('template-parts/masonry_showcase'); ?>

				<!-- Newsletter Signup -->

				<?php get_template_part('template-parts/ihdf_newsletter'); ?>

				<!-- Testimonial Slider -->

				<?php 
				$testimonial_slider_with_submit_button = true;
				include(locate_template('template-parts/testimonial_slider.php'));
				?>

				<!-- Submit Your Dream Popup Form -->

				<section class="popup_form_container">
					<div class="container">
						<div class="popup_form">
							<!-- Dream Accounts Contact Form -->
							<?php gravity_form('Submit Your Dream', false, false, false, '', true, 12); ?>
							<span class="close_icon"></span>
						</div>
					</div>
				</section>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

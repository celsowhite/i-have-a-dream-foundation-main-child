<?php
/*
Template Name: Our Impact
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<!-- Content -->
				
				<div class="container wysiwyg">
					<?php the_content(); ?>
				</div>

				<!-- Testimonial Slider -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>

				<!-- Logos -->

				<section class="ihdf_panel">

					<div class="container">

						<h2 class="text_center"><?php the_field('graduate_logos_header'); ?></h2>

						<ul class="graduate_logos_grid">

        					<?php foreach( get_field('graduate_logos') as $logo ): ?>

								<li><img src="<?php echo $logo['url'] ?>" /></li>

							<?php endforeach; ?>

						</ul>

					</div>

				</section>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>


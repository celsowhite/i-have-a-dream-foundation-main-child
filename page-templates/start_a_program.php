<?php
/*
Template Name: Start A Program
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container">

					<div class="wysiwyg">
						<?php the_content(); ?>
					</div>
					
					<!-- Where We Are Map -->

					<div class="map_container">
						<div id="start_a_program_map"></div>
						<div class="new_zealand"><img src="<?php echo get_stylesheet_directory_uri() . '/img/kartograph/new_zealand.svg'; ?>" /></div>
						<div id="tooltips_list"></div>
					</div>

					<div class="map_key">
						<p><span class="map_key_circle magenta"></span> = current "I Have A Dream" programs</p>
						<p><span class="map_key_circle purple"></span> = new program sites in progress</p>
					</div>

				</div>

				<!-- Testimonial Slider -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>

				<!-- Start A Program Contact Form -->

				<section id="start_a_program_form" class="ihdf_panel">
					<div class="container">
						<?php gravity_form('Start A Program', false, false, false, '', true, 12); ?>
					</div>
				</section>

				<!-- Download Our Materials -->

				<?php get_template_part('template-parts/download_materials'); ?>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>


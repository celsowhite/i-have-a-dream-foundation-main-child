<?php
/*
Template Name: College Savings Accounts
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/ihdf_page_header'); ?>

			<div class="page_content">

				<?php get_template_part('template-parts/social_share_bar'); ?>

				<div class="container wysiwyg">
					<?php the_content(); ?>
				</div>

				<!-- Testimonial Slider -->

				<?php get_template_part('template-parts/testimonial_slider'); ?>

				<div class="container">

					<!-- Fund Dream Accounts -->

					<?php the_field('csa_bottom_content'); ?>

					<!-- Dream Accounts Contact Form -->

					<div class="ihdf_panel">
						<?php gravity_form('Dream Accounts', false, false, false, '', true, 12); ?>
					</div>

				</div>

				<!-- Download Our Materials -->

				<?php 
				// Not using template part here so we have a unique gradient background;
				if(have_rows('materials')): ?>

					<section class="ihdf_panel teal_to_lime_gradient">

						<h2 class="white_text text_center">Download Our Materials</h2>

						<ul class="centered_list">

							<?php while(have_rows('materials')): the_row(); ?>

								<li><a href="<?php the_sub_field('file'); ?>" target="_blank" class="ihdf_button white"><i class="fa fa-download"></i> <?php the_sub_field('name'); ?></a></li>

							<?php endwhile; ?>

						</ul>

					</section>

				<?php endif; ?>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>


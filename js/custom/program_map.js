(function($) {

  $(document).ready(function() {

  "use strict";     
    
    /*=========
    Variables
    =========*/ 

    var markers;

    var updateMap;

    var currentProgramsSwitch = document.querySelector('.current_programs_switch');

    var pastProgramsSwitch = document.querySelector('.past_programs_switch');

    var mapDescriptionCurrent = document.querySelector('.map_description_toggle .current');

    var mapDescriptionPast = document.querySelector('.map_description_toggle .past');

    var svgUrl = wpUrls.childTheme + '/img/kartograph/usa.svg';

    var programsJSON = wpUrls.childTheme + '/js/data/programs.json';

    // Colors

    const white = '#FFFFFF';
    const grey = '#F3F3F3';
    const magenta = '#E11383';
    const purple = '#5E61B9';
    const teal = '#00B2BF';

    /*=========
    AJAX call the data
    =========*/

    $.getJSON(programsJSON, function(programs) {

      /*=========
      Variables
      =========*/

      // Save all active programs (current and new)

      const active_programs = programs.active_programs;

      // Filter for just current program data to be displayed on the 'Our Network' page

      const current_programs = active_programs.filter(program => {
        return program.status === 'current';
      });

      // Save the current program states as it's own data object

      const current_program_states = current_programs.map(program => program.state);

      // Save the past program states

      const past_program_states = programs.past_programs;

      /*=========
      Helper Functions
      =========*/

      // Transform program name into hyphenated single word

      function hyphenateName(name) {
        return name.replace(/[\W_]/g, "-").toLowerCase();
      }

      // On devices smaller than a tablet make the markers smaller in radius and remove the stroke width

      function mobileAdjustMarker() {
        const markerRadius = document.querySelectorAll('.map_marker');
        const mq = window.matchMedia( "(max-width: 768px)" );
        if(mq.matches) {
          Array.from(markerRadius).forEach(marker => {
            marker.setAttribute('r', '3');
            marker.style.strokeWidth = '0px';
          });
        }
      }

      /*====================================
      Map
      ====================================*/

      function mapLoaded(map) {

        /*=========
        Show Current Programs 
        =========*/ 

        function showCurrentPrograms() {

          // Update Active Toggle

          // pastProgramsSwitch.classList.remove('active');

          // currentProgramsSwitch.classList.add('active');

          mapDescriptionPast.classList.remove('active');

          mapDescriptionCurrent.classList.add('active');

          // Setup state styling. Current program states are filled in purple.

          map.addLayer('layer0', {
            styles: {
              // Check if the state initials exist in our data of current program states
              fill: function(d) { 
                return current_program_states.includes(d.key) ? purple : teal;
              },
              stroke: '#FFFFFF',
              'stroke-width': 1,
            },
          });

          // Remove the current program markers from the map. So there aren't duplicates if we've already called this function.

          if(markers) markers.remove();

          // Plot program markers on the map and set up event listeners for clicking and showing the tooltips

          markers = map.addSymbols({
            type: kartograph.Bubble,
            data: current_programs,
            style: 'fill:' + magenta + '; stroke:' + white + '; stroke-width: 3px',
            radius: 7,
            // clustering: 'noverlap',
            location: function(program) {
              // Check if the program has a location. If not then it is international and we will hide from US map
              if(program.long && program.lat) {
                return [parseInt(program.long, 10), parseInt(program.lat, 10)]
              }
              else {
                return [-1000, -1000];
              }
            },
            class: function(program) {return 'map_marker ' + hyphenateName(program.name)},
            click: function(program,path,event){
              const city = hyphenateName(program.name);
              const clickedTooltip = document.querySelector('.tooltip.' + city);
              const activeTooltip = document.querySelector('.tooltip.active');

              // If clicked tooltip is already active then remove it

              if(clickedTooltip.classList.contains('active')) {
                clickedTooltip.classList.remove('active');
              }
              else {
                clickedTooltip.classList.add('active');
                if(activeTooltip){
                  activeTooltip.classList.remove('active');
                }
              }

              // Set position of clicked tooltip

              clickedTooltip.style.left = event.offsetX - (clickedTooltip.offsetWidth / 2) + 'px';
              clickedTooltip.style.top = event.offsetY - clickedTooltip.offsetHeight - 20 + 'px';
            }
          });

          mobileAdjustMarker();

        }

        /*=========
        Show Past Programs
        =========*/

        function showPastPrograms() {

          // Update Active Toggle

          // currentProgramsSwitch.classList.remove('active');

          // pastProgramsSwitch.classList.add('active');

          mapDescriptionPast.classList.add('active');

          mapDescriptionCurrent.classList.remove('active');

          // Setup state styling. States that have had programs in the past are filled with purple.

          map.addLayer('layer0', {
            styles: {
              fill: function(d) { 
                return past_program_states.includes(d.key) ? purple : teal;
              },
              stroke: '#FFFFFF',
              'stroke-width': 1,
            },
          });

          // Remove the current program markers from the map.

          if(markers) markers.remove();

          // Hide any active tooltips.

          const activeTooltip = document.querySelector('.tooltip.active');
          
          if(activeTooltip){
            activeTooltip.classList.remove('active');
          }

        }

        // On initial load show current programs

        showCurrentPrograms();

        // Click handlers for showing current/past programs

        // currentProgramsSwitch.addEventListener('click', showCurrentPrograms);

        // pastProgramsSwitch.addEventListener('click', showPastPrograms);

      }

      if(document.querySelector('#map')) {
        kartograph.map('#map').loadMap(svgUrl, mapLoaded);
      }

      /*====================================
      Static Start A Program Map
      Markers of current and new programs.
      Current programs in pink. New programs in purple.
      ====================================*/

      function startAProgramMapLoaded(map) {

        // Setup state styling. Current program states are filled in purple.

        map.addLayer('layer0', {
          styles: {
            // Check if the state initials exist in our data of current program states
            fill: function(d) { 
              return teal;
            },
            stroke: '#FFFFFF',
            'stroke-width': 1,
          },
        });

        // Plot program markers on the map and set up event listeners for clicking and showing the tooltips

        markers = map.addSymbols({
          type: kartograph.Bubble,
          data: active_programs,
          style: function(program) {
            // Current programs are magenta
            if(program.status === 'current') {
              return 'fill:' + magenta + '; stroke:' + white + '; stroke-width: 3px;'
            }
            // New programs are purple
            else {
              return 'fill:' + purple + '; stroke:' + white + '; stroke-width: 3px'
            }
          },
          radius: 7,
          location: function(program) {
            // Check if the program has a location. If not then it is international and we will hide from US map
            if(program.long && program.lat) {
              return [parseInt(program.long, 10), parseInt(program.lat, 10)]
            }
            else {
              return [-1000, -1000];
            }
          },
          class: function(program) {return 'map_marker ' + hyphenateName(program.name)},
          click: function(program,path,event){
            const city = hyphenateName(program.name);
            const clickedTooltip = document.querySelector('.tooltip.' + city);
            const activeTooltip = document.querySelector('.tooltip.active');

            // If clicked tooltip is already active then remove it

            if(clickedTooltip.classList.contains('active')) {
              clickedTooltip.classList.remove('active');
            }
            else {
              clickedTooltip.classList.add('active');
              if(activeTooltip){
                activeTooltip.classList.remove('active');
              }
            }
            
            clickedTooltip.style.left = event.offsetX - (clickedTooltip.offsetWidth / 2) + 'px';
            clickedTooltip.style.top = event.offsetY - clickedTooltip.offsetHeight - 20 + 'px';
          }
        });
  
        mobileAdjustMarker();

      }

      if(document.querySelector('#start_a_program_map')) {
        kartograph.map('#start_a_program_map').loadMap(svgUrl, startAProgramMapLoaded);
      }

      /*=========
      Tooltips
      =========*/

      const tooltipsList = document.querySelector('#tooltips_list');

      // Load tooltips into HTML

      if(tooltipsList) {

        tooltipsList.innerHTML = active_programs.map(function(city){

          // Helpers to check if assets are there

          function hasImage(image) {
            return image ? `<div class="media"><img src="${image}" /></div>` : '';
          }
          function hasDescription(description) {
            return description ? `<p>${description}</p>` : '';
          }
          function hasWebsite(website) {
            return website ? `<li><a href="${website}" target="blank"><i class="fa fa-desktop"></i></a></li>` : '';
          }
          function hasFacebook(facebook) {
            return facebook ? `<li><a href="${facebook}" target="blank"><i class="fa fa-facebook"></i></a></li>` : '';
          }
          function hasTwitter(twitter) {
            return twitter ? `<li><a href="${twitter}" target="blank"><i class="fa fa-twitter"></i></a></li>` : '';
          }
          function hasLinkedin(linkedin) {
            return linkedin ? `<li><a href="${linkedin}" target="blank"><i class="fa fa-linkedin"></i></a></li>` : '';
          }
          function hasInstagram(instagram) {
            return instagram ? `<li><a href="${instagram}" target="blank"><i class="fa fa-instagram"></i></a></li>` : '';
          }

          // HTML

          return ` 
            <div class="tooltip ${hyphenateName(city.name)}">
              ${hasImage(city.img)}
              <div class="content">
                <h3>${city.name}</h3>
                <ul class="social_icons flush_left">
                  ${hasWebsite(city.website)}
                  ${hasFacebook(city.facebook)}
                  ${hasTwitter(city.twitter)}
                  ${hasLinkedin(city.linkedin)}
                  ${hasInstagram(city.instagram)}
                </ul>
              </div>
            </div>
          `;
        }).join('');
      }

    });

  });

})(jQuery);
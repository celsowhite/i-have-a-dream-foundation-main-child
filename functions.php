<?php
/**
 * IHDF functions and definitions.
 *
 * @package ihdf
 */

/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function ihdf_child_scripts() {
	
	// Default theme style

	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	// Wordpress Default Jquery
	
	if (!is_admin()) {
		wp_enqueue_script('jquery');
	}

	// Compiled SCSS File

	wp_enqueue_style( 'ihdf-child-styles', get_stylesheet_directory_uri() . '/css/child_style.min.css' );

	// Raphael JS
	
	wp_enqueue_script('raphael', get_stylesheet_directory_uri() . '/js/plugins/raphael/raphael.min.js', '', '', true);
	
	// Kartograph Interactive Map

	wp_enqueue_script('kartograph', get_stylesheet_directory_uri() . '/js/plugins/kartograph/kartograph.min.js', '', '', true);

	// Custom Scripts

	wp_enqueue_script('ihdf-child-scripts', get_stylesheet_directory_uri() . '/js/child_scripts.min.js', '', '', true);

}

add_action( 'wp_enqueue_scripts', 'ihdf_child_scripts' );

/*==========================================
INCLUDES
==========================================*/

// Program JSON file creation

require get_stylesheet_directory() . '/includes/program_data_json_converter.php';
